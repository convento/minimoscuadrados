/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimoscuadrados;

import java.awt.geom.Point2D;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;



/**
 *
 * @author Enrique
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
     @FXML
    private Button btnCalc;

    @FXML
    private LineChart<?, ?> lineGraph;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;
    
    static Puntos puntos =  new Puntos();

    static    MinimosCuadradosSegmentados minimos;
    
    static int cost = 0;
    static ArrayList<LineSegment> lineSegments;
    
    @FXML
    private TextField txtInput;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {        
        int costo = Integer.parseInt( txtInput.getText());
        
        XYChart.Series series2 =  new XYChart.Series();
        minimos = new MinimosCuadradosSegmentados(puntos.puntosCalculados, costo );
        lineSegments = minimos.solveRecursive();
        for (LineSegment l : lineSegments){
            //System.out.println(l);           
            
           
            Point2D[] calculados =new Point2D[2];
            
            double x1 = l.i;
            double y1 = 0;
            double x2 = l.j;
            double y2 = 0;
            
            //System.out.println("x1: " + x1);
            //System.out.println("x2: " + x2);
            
            y1 = l.a * x1 + l.b;
            y2 = l.a * x2 + l.b;
            /*
            String resultado = " (" + new Double(x1) + "," + new Double(x2) + ") " 
            + "   linea y = " + String.format("%.2f",l.a) + " x + "
            + String.format("%.2f ", l.b) + ",  error: " + String.format("%.2f", l.error);
            
            System.out.println(resultado);*/
            
            series2.getData().add(new XYChart.Data(Double.toString(x1), y1));
            series2.getData().add(new XYChart.Data(Double.toString(x2), y2));
              
        }
        lineGraph.getData().addAll(series2); 
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       XYChart.Series series =  new XYChart.Series();        
        
        for (Point2D punto : puntos.puntosCalculados){
            double x = punto.getX();
            double y = punto.getY();
            String xString = Double.toString(x);
            series.getData().add(new XYChart.Data(xString, y));
        }        
        lineGraph.getData().addAll(series);     
        
    }    
    
}
