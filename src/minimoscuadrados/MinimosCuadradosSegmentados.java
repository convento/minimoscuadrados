/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimoscuadrados;
import java.awt.geom.Point2D;
import java.util.ArrayList;
/**
 *
 * @author rike
 */
public class MinimosCuadradosSegmentados {
    ArrayList <LineSegment> lineSegments = new ArrayList<LineSegment>();

    Point2D[]  points;

    int      lengthPlusOne=0;  
    double   costSegment;   // this is the constant C 

    double  a[][];          //   a[i][j] and b[i][j] will be the coefficients of line y = ax + b + e 
    double  b[][];          //   for the segment (xi,..., xj)
    double  e_ij[][];       //   error for the segment (xi, ..., xj)
    double  opt[];          //   java initializes this to 0.

    
    public MinimosCuadradosSegmentados(Point2D[] points,  double costOfSegment){
        this.points = points;
	this.costSegment = costOfSegment;
	e_ij = new double[points.length][points.length];
	a    = new double[points.length][points.length];
	b    = new double[points.length][points.length];
	lengthPlusOne = 1 + points.length;
	opt  = new double[ points.length ];        
        
	computeEijAB();
    }


    public void computeEijAB(){
        double   denominator;
        
        double[]  sumX  = new double[ lengthPlusOne ];
        double[]  sumY  = new double[ lengthPlusOne ];
	double[]  sumX2 = new double[ lengthPlusOne ]; 
	double[]  sumY2 = new double[ lengthPlusOne ];
	double[]  sumXY = new double[ lengthPlusOne ];

	for (int i=0; i< points.length; i++){  // Watch out for the 'off by 1' errors
		// The sum_ variables index from 1 to N,  not 0 to N-1,
		// and sum_[0] == 0.
		sumX[i+1]  = sumX[i]  + points[i].getX();
		sumY[i+1]  = sumY[i]  + points[i].getY();
		sumX2[i+1] = sumX2[i] + points[i].getX() * points[i].getX(); 
		sumY2[i+1] = sumY2[i] + points[i].getY() * points[i].getY();
		sumXY[i+1] = sumXY[i] + points[i].getX() * points[i].getY();   
	}

	for (int i=0; i< points.length; i++){    
            for (int j = i+1; j < points.length; j++){                                //  i < j
		denominator = (Math.pow(sumX[j+1]-sumX[i],2.0) - (j + 1 - i) * (sumX2[j+1] - sumX2[i]));
		if (denominator == 0){
                    System.out.println("No single minimum exists e.g. the minimum is a line running along an infinitely long valley. ");
                    a[i][j] = 0.0;
                    b[i][j] = 0.0;
                    //    In this case,  we don't fit a line segment.  We take y == 0.
		}
		else{
                    a[i][j] = ((sumY[j+1] - sumY[i])*(sumX[j+1] - sumX[i]) 
                    - (j + 1 - i) * (sumXY[j+1] - sumXY[i]))/ denominator;
                    b[i][j] = ((sumX[j+1] - sumX[i])*(sumXY[j+1]- sumXY[i]) 
                    -  (sumX2[j+1] - sumX2[i])*(sumY[j+1] - sumY[i]) )/ denominator;
                    
                    e_ij[i][j] = (sumY2[j+1]-sumY2[i]) 
                    - 2*a[i][j]*         (sumXY[j+1]-sumXY[i])
                    - 2*b[i][j]*          (sumY[j+1] -sumY[i])
                    +   a[i][j]*a[i][j] * (sumX2[j+1]-sumX2[i])
                    + 2*a[i][j]*b[i][j] * (sumX[j+1]-sumX[i]) 
                    +   b[i][j]*b[i][j] * (j+1 - i);
		}
            }
	}
    }  

    public double computeOptRecursive(int j){		
        // if opt[j] hasn't been found yet
        if(opt[j] == 0){

        // Base cases
            if(j == 0)
                opt[0] = costSegment;
            else if(j == 1)
                opt[1] = costSegment;

            // Segmented Least Squares recursive algorithm
        else{
            opt[j] = Double.POSITIVE_INFINITY;
                for(int i = 1; i <	j + 1; i++){
                    opt[j] = opt[j] > computeOptRecursive(i-1) + e_ij[i][j] + costSegment?
                    computeOptRecursive(i-1) + e_ij[i][j] + costSegment : opt[j];

                    // Check if point 0 is a solution
                    if(opt[j] > costSegment + e_ij[0][j])
                        opt[j] = costSegment + e_ij[0][j];
                }
            }
        }
        return opt[j];
    }

    //  This will compute lineSegments, which is an ArrayList<LineSegment>. 	
    public void computeSegmentation(int j){

	LineSegment tempLineSegment = new LineSegment(); // temporary LineSegment
		
	// if point 0 is a solution for opt[j], add the segment 0-j
	// and stop the algorithm
	if(opt[j] == costSegment + e_ij[0][j]){
	// Initialize a LineSegment
            tempLineSegment.i = 0;
            tempLineSegment.j = j;
            tempLineSegment.a = a[0][j];
            tempLineSegment.b = b[0][j];
            tempLineSegment.error = e_ij[0][j];
            // Add the LineSegment to the ArrayList
            lineSegments.add(tempLineSegment);
	}
	else{
            // Check all i from i = 1 to i <= j, if it is add the segment
            // i-j to the ArrayList and computeSegmentation(i-1)
            for(int i = 1; i < j + 1; i++){
                if(opt[j] == opt[i - 1] + e_ij[i][j] + costSegment){
                    tempLineSegment.i = i;
                    tempLineSegment.j = j;
                    tempLineSegment.a = a[i][j];
                    tempLineSegment.b = b[i][j];
                    tempLineSegment.error = e_ij[i][j];
                    // Add the LineSegment to the ArrayList
                    lineSegments.add(tempLineSegment);
					
                    // Call computeSegmentation recursively
                    computeSegmentation(i-1);
                    break;
                }
            }
	}
    }


    public ArrayList<LineSegment> solveRecursive(){

        System.out.println("\nCalculando...\n");
	computeOptRecursive( points.length - 1);
	computeSegmentation( points.length - 1);  //  indices of points is 0, ...,  N-1
	return(lineSegments);
    }
    
    
}
/*
class LineSegment{
    int i,j;  
    double a,b,error;  

    public String toString(){
    return  " (" + new Integer(i) + "," + new Integer(j) + ") " 
            + "   line is  y = " + String.format("%.2f",a) + " x + "
            + String.format("%.2f ", b) + ",  error is " + String.format("%.2f", error);
    }    
}*/
