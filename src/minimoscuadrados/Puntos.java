/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimoscuadrados;

import java.awt.geom.Point2D;
import java.util.Random;


/**
 *
 * @author Enrique
 */
public class Puntos {
    
    public Point2D[] puntosCalculados;
    static int N = 50;
    
    public Puntos(){
        puntosCalculados = CalcularPuntos();
    }  
   
    
    public  Point2D[] CalcularPuntos() {
        int  costSegment = 10; //  Implement a slider to allow user to adjust this interactively.		
        Point2D[] points = new Point2D[N];    
	double error, a, b;
	int scaleError = 2;

	Random rand = new Random(); 

	for (int i = 0;  i < N/2; i++){
            a = 1;
            b = 10;
            points[i] = new Point2D.Float();
            error = rand.nextDouble()*2 - 1;   //  random number in [-1,1]
            points[i].setLocation(i * 1.0, a*i + b + scaleError*error);   //  y = x^2  
	}
		
	for (int i = N/4;  i < 3*N/4; i++){
            a = 0;
            b = N/2;
            points[i] = new Point2D.Float();
            error = rand.nextDouble()*2 - 1;   //  random number in [-1,1]
            points[i].setLocation(i * 1.0, a*i + b + scaleError*error);   //  y = x^2  
	}
		
	for (int i = 3*N/4;  i < N; i++){
            a = -1.2;
            b = 1.6*N;
            points[i] = new Point2D.Float();
            error = rand.nextDouble()*2 - 1;   //  random number in [-1,1]
            points[i].setLocation(i * 1.0, a*i + b + scaleError*error);   //  y = x^2  
	}		
	
        return points;
    }   
}
